# git-ci-docker-samtools

A sample project to illustrate how to user git + docker + CI.

Simple docker file to build a container with samtool.

# Build the image:

`docker build -t mysamtools .`


# Run samtools:

`docker run --rm  mysamtools samtools `

*--rm*: tell docker to remove the container once the command is over